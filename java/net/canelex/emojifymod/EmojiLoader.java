package net.canelex.emojifymod;

import java.awt.image.BufferedImage;
import net.minecraft.client.resources.data.IMetadataSection;
import java.util.Collections;
import java.util.Set;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;

import net.minecraft.client.resources.data.MetadataSerializer;
import net.minecraft.util.ResourceLocation;
import java.io.File;
import net.minecraft.client.resources.IResourcePack;

import javax.annotation.Nullable;

public class EmojiLoader implements IResourcePack
{
    private File emojiDir;
    
    public EmojiLoader(final File emojiDir) {
        this.emojiDir = emojiDir;
    }
    
    public InputStream getInputStream(final ResourceLocation location) throws IOException {
        final File emojiFile = new File(this.emojiDir, location.getResourcePath());
        return new FileInputStream(emojiFile);
    }
    
    public boolean resourceExists(final ResourceLocation location) {
        try {
            return this.getInputStream(location) != null;
        }
        catch (IOException ex) {
            return false;
        }
    }
    
    public Set<String> getResourceDomains() {
        return Collections.<String>singleton("emojis");
    }

    @Nullable
    @Override
    public <T extends IMetadataSection> T getPackMetadata(MetadataSerializer metadataSerializer, String metadataSectionName) throws IOException {
        return null;
    }

    public String getPackName() {
        return "emojify";
    }

    
    public BufferedImage getPackImage() throws IOException {
        return null;
    }
}
