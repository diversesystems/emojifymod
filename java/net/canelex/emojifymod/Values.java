package net.canelex.emojifymod;

import java.util.Iterator;
import net.minecraftforge.common.config.Property;
import com.google.common.collect.Maps;

import java.util.LinkedHashMap;
import java.util.Map;
import net.minecraftforge.common.config.Configuration;
import java.io.File;

public class Values
{
    private File emojiDir;
    private Configuration config;
    private Map<String, Emoji> emojis;
    private String current;
    public boolean enabled;
    public int comboHits;
    public int comboTime;
    
    public Values(final File emojiDir) {
        this.emojiDir = emojiDir;
        this.config = new Configuration(new File(emojiDir, "config.cfg"));
        this.emojis = new LinkedHashMap<>();
        this.reloadEmojis();
    }
    
    public void loadImage(final int slots) {
        if (this.emojis.size() == 0) {
            return;
        }
        final String[] keys = (String[])this.emojis.keySet().<String>toArray(new String[this.emojis.size()]);
        for (int i = 0; i < keys.length; ++i) {
            if (keys[i].equals(this.current)) {
                int index = (i + slots) % keys.length;
                if (index < 0) {
                    index += keys.length;
                }
                this.current = keys[index];
                break;
            }
        }
    }
    
    public void reloadEmojis() {
        this.emojis.clear();
        this.emojis.put("default1.png", new Emoji("default1.png", true));
        this.emojis.put("default2.png", new Emoji("default2.png", true));
        this.emojis.put("default3.png", new Emoji("default3.png", true));
        if (!this.emojiDir.exists()) {
            this.emojiDir.mkdir();
        }
        for (final String file : this.emojiDir.list()) {
            if (file.endsWith(".png") || file.endsWith(".jpg")) {
                this.emojis.put(file, new Emoji(file, false));
            }
        }
        this.loadConfig();
        if (!this.emojis.containsKey(this.current)) {
            this.current = "default1.png";
        }
    }
    
    public Emoji getCurrent() {
        if (this.current == null) {
            return null;
        }
        return this.emojis.get(this.current);
    }
    
    public void loadConfig() {
        this.config.load();
        this.updateConfig(true);
    }
    
    public void saveConfig() {
        this.updateConfig(false);
        this.config.save();
    }
    
    private void updateConfig(final boolean load) {
        Property prop = this.config.get("General", "enabled", true);
        if (load) {
            this.enabled = prop.getBoolean();
        }
        else {
            prop.set(this.enabled);
        }
        prop = this.config.get("General", "current", "default1.png");
        if (load) {
            this.current = prop.getString();
        }
        else {
            prop.set(this.current);
        }
        prop = this.config.get("General", "comboHits", 0);
        if (load) {
            this.comboHits = prop.getInt();
        }
        else {
            prop.set(this.comboHits);
        }
        prop = this.config.get("General", "comboTime", 5);
        if (load) {
            this.comboTime = prop.getInt();
        }
        else {
            prop.set(this.comboTime);
        }
        for (final Emoji emoji : this.emojis.values()) {
            prop = this.config.get("Emoji", emoji.name, emoji.toString());
            if (load) {
                try {
                    final String[] args = prop.getString().split(",");
                    emoji.offX = Integer.parseInt(args[0]);
                    emoji.offY = Integer.parseInt(args[1]);
                    emoji.size = Integer.parseInt(args[2]);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            else {
                prop.set(emoji.toString());
            }
        }
    }
}
