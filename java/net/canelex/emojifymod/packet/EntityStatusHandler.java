package net.canelex.emojifymod.packet;

import net.minecraft.network.play.server.SPacketEntityStatus;
import net.minecraft.world.World;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.client.Minecraft;
import net.canelex.emojifymod.EmojifyMod;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EntityStatusHandler extends ChannelInboundHandlerAdapter
{
    private EmojifyMod mod;
    private Minecraft mc;
    
    public EntityStatusHandler(final EmojifyMod mod) {
        this.mod = mod;
        this.mc = Minecraft.getMinecraft();
    }
    
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {
        if (msg instanceof SPacketEntityStatus) {
            final SPacketEntityStatus packet = (SPacketEntityStatus)msg;
            if (packet.getOpCode() == 2) {
                this.mod.onEntityDamage(packet.getEntity((World)this.mc.world));
            }
        }
        super.channelRead(ctx, msg);
    }
}
