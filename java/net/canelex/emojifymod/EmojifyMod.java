package net.canelex.emojifymod;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;
import net.minecraftforge.client.event.RenderPlayerEvent;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelHandler;
import net.canelex.emojifymod.packet.EntityStatusHandler;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraft.command.ICommand;
import net.canelex.emojifymod.command.CommandEmojify;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import java.util.List;
import java.io.File;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import java.util.UUID;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "emojifymod", name = "Emojify Mod", version = "1.0")
public class EmojifyMod
{
    private Minecraft mc;
    private TextureManager tm;
    private RenderManager rm;
    private Values values;
    private UUID targetPlayer;
    private int currentCombo;
    private long timeExpireAttack;
    private long timeExpireRender;
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        this.mc = Minecraft.getMinecraft();
        this.tm = this.mc.getTextureManager();
        this.rm = this.mc.getRenderManager();
        final File imagesDir = new File(this.mc.mcDataDir, "emojis");
        if (!imagesDir.exists()) {
            imagesDir.mkdir();
        }
        final List defaultPacks = (List)ObfuscationReflectionHelper.getPrivateValue((Class)Minecraft.class, (Object)this.mc, new String[] { "defaultResourcePacks", "field_110449_ao" });
        defaultPacks.add(new EmojiLoader(imagesDir));
        this.mc.refreshResources();
        (this.values = new Values(imagesDir)).loadConfig();
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandEmojify(this.values));
    }
    
    @SubscribeEvent
    public void onPlayerAttack(final AttackEntityEvent event) {
        if (event.getTarget() instanceof EntityPlayer) {
            final UUID uuid = event.getTarget().getUniqueID();
            if (!uuid.equals(this.targetPlayer)) {
                this.currentCombo = 0;
            }
            this.targetPlayer = uuid;
            this.timeExpireAttack = System.currentTimeMillis() + 2000L;
        }
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (this.mc.getConnection() != null) {
            final ChannelPipeline pipeline = this.mc.getConnection().getNetworkManager().channel().pipeline();
            if (pipeline.get("entity_status_handler") == null && pipeline.get("packet_handler") != null) {
                pipeline.addBefore("packet_handler", "entity_status_handler", (ChannelHandler)new EntityStatusHandler(this));
            }

        }
    }
    
    @SubscribeEvent
    public void onRenderPlayer(final RenderPlayerEvent.Post event) {
        final EntityPlayer tPlayer = (EntityPlayer)this.mc.player;
        final EntityPlayer rPlayer = event.getEntityPlayer();
        if (this.values.enabled && this.values.getCurrent() != null && !rPlayer.equals((Object)tPlayer) && (this.values.comboHits == 0 || (rPlayer.getUniqueID() == this.targetPlayer && System.currentTimeMillis() <= this.timeExpireRender))) {
            final Emoji currentImage = this.values.getCurrent();
            final double dist = Math.sqrt(event.getX() * event.getX()  + event.getZ() * event.getZ());
            if (dist <= 1.0) {
                return;
            }
            GL11.glPushMatrix();
            final double yaw = Math.atan2(event.getZ(), event.getX());
            final double pitch = Math.atan2(event.getY(), dist);
            final double distance = 0.8;
            final double factorX = -Math.cos(yaw) * Math.cos(pitch);
            final double factorZ = -Math.sin(yaw) * Math.cos(pitch);
            final double factorY = -Math.sin(pitch);
            GL11.glTranslated(event.getX(), event.getY() + rPlayer.getEyeHeight() - 0.2, event.getZ());
            GL11.glTranslated(factorX * distance, factorY * distance, factorZ * distance);
            GL11.glRotated((double)(-this.rm.playerViewY), 0.0, 1.0, 0.0);
            GL11.glRotated((double)this.rm.playerViewX, 1.0, 0.0, 0.0);
            GL11.glRotated(180.0, 0.0, 0.0, 0.0);
            GL11.glScaled(-0.02666667, -0.02666667, 0.02666667);
            GL11.glTranslated(0.0, 9.374999, 0.0);
            GL11.glTranslated((double)(-currentImage.offX), (double)(-currentImage.offY), 0.0);
            if (rPlayer.isSneaking()) {
                GL11.glTranslated(0.0, -8.0, 0.0);
            }
            GlStateManager.disableLighting();
            GlStateManager.depthMask(true);
            GlStateManager.enableBlend();
            GlStateManager.enableTexture2D();
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            final ResourceLocation resourceLocation = currentImage.getResourceLocation();
            this.tm.bindTexture(resourceLocation);
            final Tessellator tess = Tessellator.getInstance();
            final BufferBuilder wr = tess.getBuffer();
            wr.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            final int size = currentImage.size;
            wr.pos((double)(-size), (double)(-size), 0.0).tex(1.0, 1.0).color(255, 255, 255, 255).endVertex();
            wr.pos((double)(-size), (double)size, 0.0).tex(1.0, 0.0).color(255, 255, 255, 255).endVertex();
            wr.pos((double)size, (double)size, 0.0).tex(0.0, 0.0).color(255, 255, 255, 255).endVertex();
            wr.pos((double)size, (double)(-size), 0.0).tex(0.0, 1.0).color(255, 255, 255, 255).endVertex();
            tess.draw();
            GlStateManager.enableLighting();
            GlStateManager.disableBlend();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.popMatrix();
        }
    }
    
    public void onEntityDamage(final Entity entity) {
        if (entity == null) {
            return;
        }
        if (!entity.equals((Object)this.mc.player)) {
            final UUID uuid = entity.getUniqueID();
            final long currentTime = System.currentTimeMillis();
            if (uuid == this.targetPlayer && currentTime < this.timeExpireAttack) {
                ++this.currentCombo;
                if (this.currentCombo >= this.values.comboHits) {
                    this.timeExpireRender = currentTime + this.values.comboTime * 1000L;
                }
            }
        }
        else {
            this.currentCombo = 0;
        }
    }
}
