package net.canelex.emojifymod.command;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.emojifymod.gui.GuiScreenMain;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.CommandException;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommandSender;
import net.canelex.emojifymod.Values;
import net.minecraft.command.CommandBase;

public class CommandEmojify extends CommandBase
{
    private Values values;
    
    public CommandEmojify(final Values values) {
        this.values = values;
    }
    
    public String getName() {
        return "emojify";
    }
    
    public String getUsage(final ICommandSender sender) {
        return "/emojify";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        MinecraftForge.EVENT_BUS.register((Object)this);
    }

    public boolean checkPermission(MinecraftServer server, ICommandSender sender)
    {
        return true;
    }

    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GuiScreenMain(this.values));
    }
}
