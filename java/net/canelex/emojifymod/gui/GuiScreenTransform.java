package net.canelex.emojifymod.gui;

import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.canelex.emojifymod.Emoji;
import net.canelex.emojifymod.Values;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenTransform extends GuiScreen
{
    private Values values;
    private Emoji emoji;
    private GuiScreen parent;
    private GuiSlider sliderSize;
    private GuiSlider sliderX;
    private GuiSlider sliderY;
    private int middleX;
    private int middleY;
    
    public GuiScreenTransform(final Values values, final GuiScreen parent) {
        this.values = values;
        this.emoji = values.getCurrent();
        this.parent = parent;
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.drawDefaultBackground();
        this.emoji.size = this.sliderSize.getValueInt();
        this.emoji.offX = this.sliderX.getValueInt();
        this.emoji.offY = this.sliderY.getValueInt();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    
    public void initGui() {
        this.middleX = this.width / 2;
        this.middleY = this.height / 2;
        this.buttonList.add(new GuiButton(0, this.middleX - 55, this.middleY - 60, 110, 20, "Reset"));
        this.buttonList.add(this.sliderX = new GuiSlider(1, this.middleX - 55, this.middleY - 35, 110, 20, "PosX: ", "px", -16.0, 16.0, this.emoji.offX, false, true));
        this.buttonList.add(this.sliderY = new GuiSlider(2, this.middleX - 55, this.middleY - 10, 110, 20, "PosY: ", "px", -16.0, 16.0, this.emoji.offY, false, true));
        this.buttonList.add(this.sliderSize = new GuiSlider(3, this.middleX - 55, this.middleY + 15, 110, 20, "Size: ", "px", 0.0, 32.0, this.emoji.size, false, true));
        this.buttonList.add(new GuiButton(4, this.middleX - 55, this.middleY + 40, 110, 20, "Done"));
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                this.emoji.size = 16;
                this.sliderSize.setValue(16.0);
                this.sliderSize.updateSlider();
                this.emoji.offX = 0;
                this.sliderX.setValue(0.0);
                this.sliderX.updateSlider();
                this.emoji.offY = 0;
                this.sliderY.setValue(0.0);
                this.sliderY.updateSlider();
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(this.parent);
                break;
            }
        }
    }
    
    public void onGuiClosed() {
        this.values.saveConfig();
    }
}
