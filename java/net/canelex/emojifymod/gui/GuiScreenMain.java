package net.canelex.emojifymod.gui;

import java.io.IOException;

import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.gui.GuiButton;
import net.canelex.emojifymod.Values;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenMain extends GuiScreen
{
    private Values values;
    private GuiSlider sliderCombo;
    private GuiSlider sliderTime;
    private int middleX;
    private int middleY;
    
    public GuiScreenMain(final Values values) {
        (this.values = values).reloadEmojis();
    }
    
    public void initGui() {
        this.middleX = this.width / 2;
        this.middleY = this.height / 2;
        this.buttonList.add(new GuiButton(0, this.middleX - 55, this.middleY - 80, 20, 20, "<"));
        this.buttonList.add(new GuiButton(1, this.middleX + 35, this.middleY - 80, 20, 20, ">"));
        this.buttonList.add(new GuiButton(2, this.middleX - 55, this.middleY - 35, 110, 20, this.getEnabledText()));
        this.buttonList.add(new GuiButton(3, this.middleX - 55, this.middleY - 10, 110, 20, "Size and Position"));
        this.buttonList.add(this.sliderCombo = new GuiSlider(4, this.middleX - 55, this.middleY + 15, 110, 20, "Combo: ", "", 0.0, 10.0, this.values.comboHits, false, true));
        this.buttonList.add(this.sliderTime = new GuiSlider(5, this.middleX - 55, this.middleY + 40, 110, 20, "Time: ", "", 1.0, 10.0, this.values.comboTime, false, true));
        this.buttonList.add(new GuiButton(6, this.middleX - 55, this.middleY + 65, 110, 20, "Done"));
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.drawDefaultBackground();
        drawRect(this.middleX - 30, this.middleY - 100, this.middleX + 30, this.middleY - 40, 1996488704);
        if (this.values.getCurrent() != null) {
            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.mc.getTextureManager().bindTexture(this.values.getCurrent().getResourceLocation());
            drawModalRectWithCustomSizedTexture(this.middleX - 25, this.middleY - 95, 0.0f, 0.0f, 50, 50, 50.0f, 50.0f);
        }
        this.values.comboHits = this.sliderCombo.getValueInt();
        this.values.comboTime = this.sliderTime.getValueInt();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                this.values.loadImage(-1);
                break;
            }
            case 1: {
                this.values.loadImage(1);
                break;
            }
            case 2: {
                this.values.enabled = !this.values.enabled;
                button.displayString = this.getEnabledText();
                break;
            }
            case 3: {
                this.mc.displayGuiScreen((GuiScreen)new GuiScreenTransform(this.values, this));
                break;
            }
            case 6: {
                this.mc.displayGuiScreen((GuiScreen)null);
                break;
            }
        }
    }
    
    public void onGuiClosed() {
        this.values.saveConfig();
    }
    
    private String getEnabledText() {
        if (this.values.enabled) {
            return "Enabled: " + TextFormatting.GREEN + "TRUE";
        }
        return "Enabled: " + TextFormatting.RED + "FALSE";
    }
}
